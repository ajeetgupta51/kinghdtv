     <!-- Main Footer -->
      <footer class="main-footer">
        <!-- To the right -->
        <div class="pull-right hidden-xs">
          Anything you want
        </div>
        <!-- Default to the left -->
        <strong>Copyright &copy; 2015 <a href="#">Company</a>.</strong> All rights reserved.
      </footer>
     <!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
    </div><!-- ./wrapper -->

    <!-- REQUIRED JS SCRIPTS -->

    <!-- jQuery 2.1.4 -->
    <script src="<?php echo base_url(); ?>assets/dashboard/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?php echo base_url(); ?>assets/dashboard/bootstrap/js/bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url(); ?>assets/dashboard/dist/js/app.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/dashboard/dist/js/alertify.js"></script>
    <script src="<?php echo base_url(); ?>assets/dashboard/dist/js/dashboard.js"></script>
    
 


<!-- //Read more: http://www.thesoftwareguy.in/multiple-dropdown-with-jquery-ajax-and-php/#ixzz46drXmnm9 -->
     <script type="text/javascript">
    //when we delete any row with table then this message function are run
     $(".confirm").click("click", function () {
      var urls = $(this).attr('data-id');
     alertify.confirm("Are You Sure That You Want To Delete This?", function (asc) {
         if (asc) {
           //alertify.success("Record is deleted.");
         window.location.href = urls; 
             //ajax call for delete       
             //alertify.success("You clicked Ok.");

         }
     }, "Default Value");
     return false;

 });
    // success notification message when eny php query are run
    <?php  $data = $this->session->flashdata('responsemsg');
        if($this->session->flashdata('responsemsg') && $data['Status']=='success'){

            echo 'alertify.success("'.$data['msg'].'")';
        }elseif($this->session->flashdata('responsemsg') && $data['Status']=='error'){
            echo 'alertify.error("Sorry:'.$data['msg'].'")';
        }
      ?>   
      </script>
  </body>
</html>
