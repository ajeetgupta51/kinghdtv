

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Channel Category
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Category</a></li>
            <li class="active">Channel Category</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
         <div class="box box-success">
          <?php if($this->input->get('add')){ ?>
         <div class="box-header ui-sortable-handle">
           <a href="<?php echo base_url('new_channel'); ?>" class="btn btn-success pull-right" style="margin: 1px;">View Category</a>
         </div>

           <div class="content">
             <form id="course-form" method="post"  enctype="multipart/form-data">
                  <div class="row">
                    <div class="col-sm-4 col-xs-4">
                        <div class="form-group field-batches-start_date required">
                            <label class="control-label" for="batches-start_date">Results List CategoryID Type:</label>
                             <input type="text" class="form-control" name="results_list_CategoryID_type" value='<?php if(isset($editItem)){ echo $editItem['results_list_CategoryID_type']; }else{ echo set_value('results_list_CategoryID_type'); } ?>'>
                            <span class="text-danger"><?php echo form_error('results_list_CategoryID_type');  ?></span>
                        </div> 
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <div class="form-group field-batches-start_date required">
                            <label class="control-label" for="batches-start_date">Results List CategoryID Class Name:</label>
                            <input type="text" class="form-control" name="results_list_CategoryID_className" value='<?php if(isset($editItem)){ echo $editItem['results_list_CategoryID_className']; }else{ echo set_value('results_list_CategoryID_className'); } ?>'>
                            <span class="text-danger"><?php echo form_error('results_list_CategoryID_className');  ?></span>
                        </div>
                    </div>
                    <div class="col-sm-4" id="shown">
                          <div class="form-group field-batches-batch_name required">
                           <label class="control-label" for="batches-start_date">Results List CategoryID ObjectId:</label>
                           <input type="text" class="form-control" name="results_list_CategoryID_objectId" value='<?php if(isset($editItem)){ echo $editItem['results_list_CategoryID_objectId']; }else{ echo set_value('results_list_CategoryID_objectId'); } ?>'>
                           <span class="text-danger"><?php echo form_error('results_list_CategoryID_objectId');  ?></span>
                          </div>
                      </div>
                 </div>
                <!-- new row here -->
                <div class="row">
                    <div class="col-sm-4">
                          <div class="form-group field-batches-batch_name required">
                           <label class="control-label" for="batches-start_date">Results List Channel Links:</label>
                            <input type="text" class="form-control" name="results_list_ChannelLinks" value='<?php if(isset($editItem)){ echo $editItem['results_list_ChannelLinks']; }else{ echo set_value('results_list_ChannelLinks'); } ?>'>
                            <span class="text-danger"><?php echo form_error('results_list_ChannelLinks');  ?></span>
                          </div>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <div class="form-group field-batches-start_date required">
                            <label class="control-label">Results List Channel Name:</label>
                            <input type="text" class="form-control" name="results_list_ChannelName" value='<?php if(isset($editItem)){ echo $editItem['results_list_ChannelName']; }else{ echo set_value('results_list_ChannelName'); } ?>'>
                            <span class="text-danger"><?php echo form_error('results_list_ChannelName'); ?></span>
                        </div>
                    </div>
                     <div class="col-sm-4 col-xs-4">
                        <div class="form-group field-batches-start_date required">
                            <label class="control-label">Results List CatObjectId:</label>
                            <input type="text" class="form-control" name="results_list_catObjectId" value='<?php if(isset($editItem)){ echo $editItem['results_list_catObjectId']; }else{ echo set_value('results_list_catObjectId'); } ?>'>
                            <span class="text-danger"><?php echo form_error('results_list_catObjectId'); ?></span>
                        </div>
                    </div>
                </div>
                <!-- new row here -->
                
                <div class="row">
                    <div class="col-sm-4">
                          <div class="form-group field-batches-batch_name required">
                           <label class="control-label" for="results_list_isFromOwnServer">Results List IsFromOwnServer:</label>
                            <input type="text" class="form-control" name="results_list_isFromOwnServer" value='<?php if(isset($editItem)){ echo $editItem['results_list_isFromOwnServer']; }else{ echo set_value('results_list_isFromOwnServer'); } ?>'>
                            <span class="text-danger"><?php echo form_error('results_list_isFromOwnServer');  ?></span>
                          </div>
                    </div>
                    <div class="col-sm-4 col-xs-4">
                        <div class="form-group field-batches-start_date required">
                            <label class="control-label">Results List ObjectId:</label>
                            <input type="text" class="form-control" name="results_list_objectId" value='<?php if(isset($editItem)){ echo $editItem['results_list_objectId']; }else{ echo set_value('results_list_objectId'); } ?>'>
                            <span class="text-danger"><?php echo form_error('results_list_objectId'); ?></span>
                        </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-sm-4 col-xs-4">
                    <label class="control-label"><hr></label>
                      <button type="submit" class="btn btn-primary btn-create">SUBMIT</button> &nbsp;&nbsp;&nbsp;&nbsp;<button type="reset" name="reset" class="btn btn-danger btn-create">CANCEL</button>
                  </div>
                </div>
          </form>
          <!-- category listing start -->
          <?php }else{ ?>
          <div class="box-header ui-sortable-handle">
           <h4></h4><a href="<?php echo base_url('new_channel?add='); ?>1" class="btn btn-success pull-right">Add Categorey</a>
         </div>
          <div class="box-body">
          <div class="row">
                    <div id="printable" class="col-md-12">
                        <table class="table table-striped table-condensed table-hover" style="margin-bottom:5px;">
                            <thead>
                                <tr class="active" role="row">
                                    <th>Results List CategoryID Class Name</th>
                                    <th>Results List CategoryID ObjectId</th>
                                    <th>Results List Channel Links</th>
                                    <th>Results List Channel Name</th>
                                    <th>Results List CatObjectId</th>
                                    <th>Results List ObjectId</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody role="alert" aria-live="polite" aria-relevant="all">
                            <?php $i=1;  if(isset($list_item)){
                                foreach($list_item as $row){
                             ?>
                                <tr>
                                    <td><?php echo $row['results_list_CategoryID_className']; ?></td>
                                    <td><?php echo $row['results_list_CategoryID_objectId']; ?></td>
                                    <td><?php echo $row['results_list_ChannelLinks']; ?></td>
                                    <td><?php echo $row['results_list_ChannelName']; ?></td>
                                    <td><?php echo $row['results_list_catObjectId']; ?></td>
                                    <td><?php echo $row['results_list_objectId']; ?></td>
                                    <td>
                                       <a href="<?=base_url('new_channel?add='.$row['id'].'&update='.$row['id']); ?>" class="tip btn btn-success btn-xs"><i class="fa fa-eye"></i>
                                       </a>
                                       <a href="#" id="confirm" data-id="<?php echo base_url('new_channel?delete='.$row['id']); ?>" class="tip btn btn-danger btn-xs confirm"><i class="fa fa-trash-o"></i>
                                       </a>
                                       
                                    </td>
                                </tr>
                      <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
             </div>
             <?php } ?>
             </div>
             </div>
          <!-- category listing End -->
         </div>
          <!-- Your Page Content Here -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

 