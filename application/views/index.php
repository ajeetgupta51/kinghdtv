

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        
        <section class="content-header">
          <h1>Dashboard</h1>
          <ul>
          </ul>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="mini-stat clearfix bg-facebook rounded">
                                <span class="mini-stat-icon"><i class="fa fa-facebook fg-facebook"></i></span><!-- /.mini-stat-icon -->
                                <div class="mini-stat-info">
                                    <span><?php if(isset($userdetails)){ echo $userdetails->getFollowers();} ?></span>
                                    Facebook Like
                                </div><!-- /.mini-stat-info -->
                            </div><!-- /.mini-stat -->
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="mini-stat clearfix bg-twitter rounded">
                                <span class="mini-stat-icon"><i class="fa fa-twitter fg-twitter"></i></span><!-- /.mini-stat-icon -->
                                <div class="mini-stat-info">
                                    <span>7,153</span>
                                    Twitter Followers
                                </div><!-- /.mini-stat-info -->
                            </div><!-- /.mini-stat -->
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="mini-stat clearfix bg-googleplus rounded">
                                <span class="mini-stat-icon"><i class="fa fa-google-plus fg-googleplus"></i></span><!-- /.mini-stat-icon -->
                                <div class="mini-stat-info">
                                    <span>793</span>
                                    Google+ Posts
                                </div><!-- /.mini-stat-info -->
                            </div><!-- /.mini-stat -->
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <div class="mini-stat clearfix bg-bitbucket rounded">
                                <span class="mini-stat-icon"><i class="fa fa-bitbucket fg-bitbucket"></i></span><!-- /.mini-stat-icon -->
                                <div class="mini-stat-info">
                                    <span>8,932</span>
                                    Repository
                                </div>
                            </div><!-- /.mini-stat-info -->
                        </div><!-- /.mini-stat -->
                    </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

 