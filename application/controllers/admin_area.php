<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_area extends CI_Controller {
     function __construct(){
        parent::__construct();     
        $this->load->library('upload');
        }
 

/////////////////////////////////////////
//////////   Dashboard Page    //////////
/////////////////////////////////////////
    public function index(){
        $data['title']="Dashboard";
        $this->render_view('index',$data);
    }

/////////////////////////////////////////
//////////   Category Page     //////////
/////////////////////////////////////////
     public function channel_new(){
      $data['title']="Category";
      if($this->input->get('delete')){
            $this->db->where('id', $this->input->get('delete'));
            $this->db->delete('categorychannelsnew');
            $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Delete Successfully '));
            redirect(base_url('new_channel'));
        }
        if($this->input->get('update')){
          $UID=$this->input->get('update');
            $data['editItem'] = $previous = $this->db->query("SELECT * from categorychannelsnew where `id` = '$UID'")->row_array();
        }
        if($this->input->server('REQUEST_METHOD')=="POST"){
            $this->form_validation->set_rules('results_list_CategoryID_type', 'Titles', 'trim|required');
            $this->form_validation->set_rules('results_list_CategoryID_className', ' ', 'required');
            $this->form_validation->set_rules('results_list_CategoryID_objectId', ' ', 'required');
            $this->form_validation->set_rules('results_list_ChannelLinks', ' ', 'required');
            $this->form_validation->set_rules('results_list_ChannelName', ' ', 'required');
            $this->form_validation->set_rules('results_list_catObjectId', ' ', 'required');
            $this->form_validation->set_rules('results_list_objectId', ' ', 'required');
            $this->form_validation->set_rules('results_list_isFromOwnServer', ' ', 'required');
            if($this->form_validation->run() ==  TRUE ){
            $data = array(
                 'results_list' => $this->input->post('results_list'),
                 'results_list_CategoryID_type' =>$this->input->post('results_list_CategoryID_type'),
                 'results_list_CategoryID_className'=> $this->input->post('results_list_CategoryID_className'),
                 'results_list_CategoryID_objectId' => $this->input->post('results_list_CategoryID_objectId'),
                 'results_list_ChannelLinks' =>$this->input->post('results_list_ChannelLinks'),  
                 'results_list_ChannelName' =>$this->input->post('results_list_ChannelName'),  
                 'results_list_catObjectId' =>$this->input->post('results_list_catObjectId'),  
                 'results_list_objectId' =>$this->input->post('results_list_objectId'),  
                 'results_list_isFromOwnServer'=>$this->input->post('results_list_isFromOwnServer'),  
                 'results_list_createdAt' => date('Y-m-d H:i:s'),
                 'results_list_updatedAt' => date('Y-m-d H:i:s')
                 );
               if($this->input->get('update')){
                 $id = $this->input->get('update');
                 $this->db->where('id', $id);
                 $Up_response = $this->db->update('categorychannelsnew', $data);
                }else{
                  $response = $this->db->insert('categorychannelsnew',$data); 
                }

                if($response){
                     $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Successfully Insert'));
                }elseif($Up_response){
                  $this->session->set_flashdata('responsemsg',array('Status'=>'success','msg'=>'Successfully Update'));
                }else{
                  $this->session->set_flashdata('responsemsg',array('Status'=>'error','msg'=>' Some error occurred. Please try again.'));
                }
                 redirect('new_channel'); 
            }//If closed validation true
            }//If closed Post
        
        $data['list_item'] = $this->db->query("SELECT * FROM `categorychannelsnew`")->result_array();
        $this->render_view('channel_new',$data);
     }

/////////////////////////////////////////////////////////////
//////////  Header, Footer, Sidebar are includes   //////////
/////////////////////////////////////////////////////////////
    private function render_view($view,$data){
        $this->load->view('admin_includes/header',$data);
        $this->load->view('admin_includes/sidebar',$data);
        $this->load->view($view,$data);
        $this->load->view('admin_includes/footer',$data);
            }
} ?>