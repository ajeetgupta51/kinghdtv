-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 17, 2016 at 05:00 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `categorychannels`
--

-- --------------------------------------------------------

--
-- Table structure for table `categorychannelsnew`
--

CREATE TABLE IF NOT EXISTS `categorychannelsnew` (
  `results_list` int(11) DEFAULT NULL,
`id` bigint(255) NOT NULL,
  `results_list_CategoryID_type` varchar(7) CHARACTER SET utf8 DEFAULT NULL,
  `results_list_CategoryID_className` varchar(8) CHARACTER SET utf8 DEFAULT NULL,
  `results_list_CategoryID_objectId` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `results_list_ChannelLinks` varchar(71) CHARACTER SET utf8 DEFAULT NULL,
  `results_list_ChannelName` varchar(22) CHARACTER SET utf8 DEFAULT NULL,
  `results_list_catObjectId` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `results_list_createdAt` varchar(24) CHARACTER SET utf8 DEFAULT NULL,
  `results_list_isFromOwnServer` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `results_list_objectId` varchar(10) CHARACTER SET utf8 DEFAULT NULL,
  `results_list_updatedAt` varchar(24) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=469 ;

--
-- Dumping data for table `categorychannelsnew`
--

INSERT INTO `categorychannelsnew` (`results_list`, `id`, `results_list_CategoryID_type`, `results_list_CategoryID_className`, `results_list_CategoryID_objectId`, `results_list_ChannelLinks`, `results_list_ChannelName`, `results_list_catObjectId`, `results_list_createdAt`, `results_list_isFromOwnServer`, `results_list_objectId`, `results_list_updatedAt`) VALUES
(0, 463, 'yhjyj', 'jjuii', 'ukioo', 'ioikhnu', 'nio', 'njumu', '2016-05-17 17:41:00', 'yhuji', 'huko45555', '2016-05-17 17:41:00'),
(0, 467, '444', '4444', '445', '5555', '5555', '5555', '2016-05-17 17:34:53', '55555', '55578555', '2016-05-17 17:34:53'),
(0, 468, 'test', 'test', 'test', 'test', 'test', 'test', '2016-05-17 17:40:31', 'test', 'test', '2016-05-17 17:40:31');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categorychannelsnew`
--
ALTER TABLE `categorychannelsnew`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categorychannelsnew`
--
ALTER TABLE `categorychannelsnew`
MODIFY `id` bigint(255) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=469;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
